import _ from 'lodash';
import User from '../models/User';

async function findAll(sortBy) {
    return sortBy ? _.sortBy(User.find(), sortBy) : User.find();
}

async function findById(id) {
    return User.find(id);
}

async function add(user) {
    return User.add(user);
}

async function update(user) {
    return User.update(user);
}

async function removeById(id) {
    return User.remove(id);
}

export default {
    add,
    findAll,
    findById,
    update,
    removeById
};