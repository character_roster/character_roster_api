import usersService from '../services/users'

async function findAll(ctx) {
    const { query: { sortBy } } = ctx;
    ctx.body = await usersService.findAll(sortBy);
}

async function findById(ctx) {
    const { id } = ctx.params;
    ctx.body = await usersService.findById(id);
}

async function add(ctx) {
    const user = ctx.request.body;
    ctx.body = await usersService.add(user);
}

async function update(ctx) {
    const user = ctx.request.body;
    ctx.body = await usersService.update(user);
}

async function removeById(ctx) {
    const { id } = ctx.params;
    ctx.body = await usersService.removeById(id);
}

export default {
    add,
    findAll,
    findById,
    update,
    removeById
};
