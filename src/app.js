import Koa from 'koa';
import http from 'http';
import gracefulShutdown from 'http-graceful-shutdown';
import config from 'config';
import bluebird from 'bluebird';
import responseTime from 'koa-response-time';
import bodyParser from 'koa-bodyparser';
import routes from './routes';

const app = new Koa();

app.use(responseTime());
app.use(bodyParser());
app.use(routes);

// Services here conform to a function that returns a promise. The promise needs to resolve to an object
// that has a "close" function that returns a promise. For example, initializing a database.
// Sample:
// function service() {
//     return Promise.resolve({close: () => Promise.resolve('Service Stopped')});
// }
const initServices = [];

async function shutdown(server, initializedServices) {
   await bluebird.promisify(server.close, { context: server })();
   return await Promise.all(initializedServices.map(s => s.close()));
}

export default {
   /* istanbul ignore next */
   async start() {
       const server = await http.createServer(app.callback());
       gracefulShutdown(server, {
           timeout: config.shutdownTimeout,
           finally: function() {
               console.info('Server gracefully shut down...');
           }
       });
       await bluebird.promisify(server.listen, { context: server })(config.port);
       const initializedServices = await Promise.all(initServices.map(s => s(app)));

       return {
           server,
           shutdown: () => shutdown(server, initializedServices)
       };
   }
};